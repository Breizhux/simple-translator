#!/usr/bin/env python3
#! -*- coding : utf-8 -*-

import setuptools

setuptools.setup(
    name = 'simple-translator',
    version = '1.0.0',
    description = 'Un outil de traduction local très simple',
	#license = "WTFPL",
    url = 'https://gitlab.com/breizhux/simple-translator',
    author = 'Breizhux',
    author_email = 'xavier.lanne@gmx.fr',
    packages = setuptools.find_packages(
        exclude = [
            'bin',
            'lib',
            '__pycache__',
            'pyvenv.cfg',
            '.gitignore',
            'requirements.txt',
        ],
    ),
    #package_data = {'pkg_name' : [
    #    'path/*',
    #]},
    #include_package_data=True,
    install_requires = [
        'tf-keras',
        'langdetect',
        'tensorflow',
        'transformers',
        'sentencepiece',
    ],
	python_requires = ">=3.8",
    entry_points = {
        'console_scripts': [
            'simple-translator  =  simple_translator.simple_translator:main',
        ],
    },
)
