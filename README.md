# Simple-translator

A very easy to use tool to make your translations locally.

## Installation

**From pip**

```
pip install git+https://gitlab.com/Breizhux/simple-translator.git
```


## Usage

**Mode cli**

```bash
simple-translator -s 'en' 'fr' "The text to translate."
```
1. The source language (default : auto)
2. The target language
3. The text to translate

**In your python code**

```python
import simple_translator

#translation
translator = simple_translator.SimpleTranslator()
french_text = translator.translate("You text to translate", 'fr', source=source, model='medium')

#set a personnalised model
translator.load_model(model_name)
```

## Description

### Models

This program use only the model created by [Helsinki-NLP](https://huggingface.co/Helsinki-NLP/).
Because its models are light but powerful enough. The only drawback is that each model can only handle two languages. So the more languages you use, the more space the templates will take up on your storage space.
