#!/usr/bin/env python3
#! -*- coding : utf-8 -*-

import os
import sys
import argparse
import requests
import warnings
from langdetect import detect
from transformers import pipeline


DEBUG = False


MODELS_NAME = {
    'base'   : "Helsinki-NLP/opus-mt-tc-base-{src}-{trg}",
    'medium' : "Helsinki-NLP/opus-mt-{src}-{trg}",
    'big'    : "Helsinki-NLP/opus-mt-tc-big-{src}-{trg}"
}
LANGUAGES = {
    'français'  : 'fr',
    'anglais'   : 'en',
    'allemand'  : 'de',
    'russe'     : 'ru',
    'turc'      : 'tr',
    'ukrainien' : 'uk',
    'polonais'  : 'pl',
    'suédois'   : 'sv',
    'vietnamien': 'vi',
}


class SimpleTranslator :
    """ A simple translator based on little Helsinki-NLP models."""

    HUGGING_BASE = "https://huggingface.co/"
    HUGGING_CACHE = f"{os.environ['HOME']}/.cache/huggingface/hub"

    def __init__(self) :
        self.__target = None
        self.__source = None
        self.__model_size = None
        #loaded transformers model
        self.__current_model_name = None
        self.__model = None


    def __set_model_size(self, model_size) :
        """ Change the current model."""
        if self.__model_size != model_size :
            del self.__model
            self.__model = None
        self.__model_size = model_size

    def __set_source(self, source) :
        """ Change the source language of model.
        Return True if available, else False and no change applicate."""
        #Verify lang available
        if len(source) != 2 and source in LANGUAGES : source = LANGUAGES[source]
        elif not source in LANGUAGES.values() : return False
        #Set lang lang available
        if self.__source != source :
            del self.__model
            self.__model = None
        self.__source = source
        return True

    def __set_target(self, target) :
        """ Change the target language of model.
        Return True if available, else False and no change applicate."""
        #Verify lang available
        if len(target) != 2 and target in LANGUAGES : target = LANGUAGES[target]
        elif not target in LANGUAGES.values() : return False
        #Set lang lang available
        if self.__target != target :
            del self.__model
            self.__model = None
        self.__target = target
        return True

    def __model_exists(self, model_id) :
        """ Return if a model exists (offline or online)."""
        #if available in local model
        model_path = f"{self.HUGGING_CACHE}/models--{model_id.replace('/', '--')}"
        if os.path.exists(model_path) : return True
        #if available in huggingface hub
        try :
            r = requests.get(f"{self.HUGGING_BASE}{model_id}")
            return r.status_code == 200
        except requests.exceptions.ConnectionError :
            warnings.warn("Network error : check availability of model only on local machine.")
            return False

    def __get_available_models(self, source, target) :
        """ Return list of available model for selected source and target language."""
        available = {} #size : model_id
        for i in MODELS_NAME :
            model_id = MODELS_NAME[i].format(src=source, trg=target)
            if self.__model_exists(model_id) :
                available[i] = model_id
        return available

    def __find_model(self, population, model_size="medium") :
        """ Return the model id corresponding to size, or the lower size model."""
        if model_size == "base" and "base" in population :
            return population["base"]
        elif model_size == "medium" :
            if "medium" in population :  return population["medium"]
            if "base" in population :    return population["base"]
        elif model_size == "big" :
            if "big" in population :     return population["big"]
            if "medium" in population :  return population["medium"]
            if "base" in population :    return population["base"]
        return False

    def __get_model(self, source, target, model_size) :
        """ Return the corresponding model name. Return False if no error, else error message."""
        #Set source and target languages
        c = (self.__set_source(source), self.__set_target(target))
        if c != (True, True) :
            message = "Les langues suivantes ne sont pas disponible : "
            if not c[0] : message += f"{source}, "
            if not c[1] : message += target
            return (1, message.strip(", "))
        #set model size
        self.__set_model_size(model_size)
        #Load model
        if self.__model is None :
            print("[model] Find a model...\r", end="")
            available = self.__get_available_models(self.__source, self.__target)
            model_id = self.__find_model(available, model_size=self.__model_size)
            if not model_id : return (2, "No model available")
            print(f"[model] Model \"{model_id}\" has been selected.")
        return (0, model_id)

    def load_model(self, model_name) :
        """ Load the model from huggingface name."""
        if self.__current_model_name is None or model_name != self.__current_model_name :
            self.__current_model_name = model_name
            self.__model = pipeline("translation", model=model_name)

    def translate(self, text, target, source='auto', model="medium") :
        """ Return the text translated."""
        target = target.lower()
        source = source.lower()
        #manage source lang (auto vs declarative)
        if source == "auto" :
            if self.__source not in ('auto', None) : source = self.__source
            else :
                print("[lang] Autodetect source language of text...\r", end="")
                source = detect(text)
                print(f"[lang] Autodetect found \"{source}\" as source language.")
        #get model name
        model = self.__get_model(source, target, model)
        if model[0] != 0 :
            print(f"[error] {model[0]} : {model[1]}")
            sys.exit(model[0])
        #load model if necessary
        self.load_model(model[1])
        #build translation with the power of transformers !!
        result = self.__model.predict(text)
        return result[0]['translation_text']



def main() :
    parser = argparse.ArgumentParser(description='Solution de traduction simple et locale en python.')

    #parser.add_argument('-h', '--help', action='help', help='Afficher l\'aide et quitter.')
    parser.add_argument('-l', '--list-languages', action='store_true', help='Afficher la liste des langues disponibles.')
    parser.add_argument('target', type=str, nargs='?', help='Langue de sortie.')
    parser.add_argument('text', type=str, nargs='?', help='Texte à traduire')
    parser.add_argument('-s', '--source', type=str, default='auto', help='Langue source (défaut: auto).')
    parser.add_argument('-m', '--model', type=str, default='medium', help='Modèle à utiliser : base, medium ou big (défaut : medium).')

    args = parser.parse_args()

    if args.list_languages:
        print("\nAvailable languages :")
        max_lenght = len(max(LANGUAGES, key=lambda k:len(k)))
        for i in LANGUAGES :
            print(f" - {i.ljust(max_lenght).capitalize()} : {LANGUAGES[i]}")
        sys.exit(0)

    print(f"Translating text from {args.source} to {args.target}...")
    translator = SimpleTranslator()
    translation = translator.translate(args.text, args.target, source=args.source, model=args.model)
    print(translation)



if __name__ == "__main__" :
    text = "Bonjour, comment allez-vous ? Ne trouvez-vous pas qu'il fait beau aujourd'hui ? C'est effectivement l'été !"
    translator = SimpleTranslator()
    translation = translator.translate(text, "en", source="fr", model="medium")
    print("[fr --> en]")
    print("Texte original :")
    print(text)
    print()
    print("Texte traduit :")
    print(translation)
